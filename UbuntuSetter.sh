#!/bin/bash

# UbuntuSetter 1.0
# Deployer for Ubuntu Server 14.04 LTS
#
# Team SIAT

# Basado en el proyecto JackTheStripper
# Credits to Eugenia Bahit

source helpers.sh

##############################################################################################################

show_banner(){
	echo
	echo "

  _____ _____       _______   _____               _                _             
 / ____|_   _|   /\|__   __| |  __ \             | |              (_)            
| (___   | |    /  \  | |    | |__) | __ ___   __| |_   _  ___ ___ _  ___  _ __  
 \___ \  | |   / /\ \ | |    |  ___/  __/ _ \ / _  | | | |/ __/ __| |/ _ \|  _ \ 
 ____) |_| |_ / ____ \| |    | |   | | | (_) | (_| | |_| | (_| (__| | (_) | | | |
|_____/|_____/_/    \_\_|    |_|   |_|  \___/ \__,_|\__,_|\___\___|_|\___/|_| |_|
                                                                                                                                            
Para Ubuntu Server 14.04 LTS
Desarrollado por TeamSiat"
echo 
echo 

}

##############################################################################################################

function check_root() {
if [ "$USER" != "root" ]; then
      echo "Permisos Denegados"
      echo "Este fichero debe ser ejecutado en root"
else
      clear
      show_banner
fi
}

##############################################################################################################

function config_host() {
    clear
    show_banner
    write_title "1. Configurar Hostname"
    echo -e "\e[93m[?]\e[00m ¿Desea configurar un hostname? (y/n): "; read config_host
    if [ "$config_host" == "y" ]; then
    	serverip=$(__get_ip)
    	echo " Ingrese un nombre para identificar a este servidor"
    	echo -n " (por ejemplo: myserver) "; read host_name
    	echo -n " ¿Cuál será el dominio principal? "; read domain_name
    	echo $host_name > /etc/hostname
    	hostname -F /etc/hostname
    	echo "127.0.0.1    localhost.localdomain      localhost" >> /etc/hosts
    	echo "$serverip    $host_name.$domain_name    $host_name" >> /etc/hosts
    fi
    say_done
}

##############################################################################################################

function sysupdate() {
    clear
    show_banner
    write_title "2. Actualización del sistema"
    apt-get update
    apt-get upgrade -y
    say_done
}

##############################################################################################################

function set_new_user() {
    clear
    show_banner
    write_title "3. Creación de un nuevo usuario"
    echo -n " Indique un nombre para el nuevo usuario: "; read username
    adduser $username
    usermod -a -G sudo $username
    say_done
}

##############################################################################################################

function give_instructions() {
    clear
    show_banner
    serverip=$(__get_ip)
    write_title "4. Generación de llave RSA en su ordenador local"
    echo " *** SI NO TIENE UNA LLAVE RSA PÚBLICA EN SU ORDENADOR, GENERE UNA ***"
    echo "     Siga las instrucciones y pulse INTRO cada vez que termine una"
    echo "     tarea para recibir una nueva instrucción"
    echo " "
    echo "     EJECUTE LOS SIGUIENTES COMANDOS:"
    echo -n "     a) ssh-keygen (Le va a solicitar un passphrase, es una clave interna de su key, recuerde esta clave)"; read foo1
    echo -n "     b) ssh-copy-id '$username@$serverip' (Le va a solicitar la contraseña del usuario $username)"; read foo2
    echo -n "     b) ssh-add (Es posible que le solicite el passphrase que indicó en el primer paso)"; read foo3
    say_done
}

##############################################################################################################

function permission_rsa() {
    clear
    show_banner
    write_title "5. Se otorgará permisos y pertenencia a las carpetas donde se ubica la llave generada en el paso 4"
    chmod 700 /home/$username/.ssh
    chmod 600 /home/$username/.ssh/authorized_keys
    chown -R $username:$username /home/$username/.ssh
    say_done
}

##############################################################################################################

function ssh_reconfigure() {
    clear
    show_banner
    write_title "6. Securizar accesos SSH"
    sed s/USERNAME/$username" USERNAME"/g templates/sshd_config > /etc/ssh/sshd_config
    service ssh restart
    say_done
}

##############################################################################################################

function set_iptables_rules() {
    clear
    show_banner
    write_title "7. Establecer reglas para iptables (firewall)"
    cat templates/iptables > /etc/iptables.firewall.rules
    iptables-restore < /etc/iptables.firewall.rules
    say_done
}

##############################################################################################################

function create_iptable_script() {
    clear
    show_banner
    write_title "8. Crear script de automatización de reglas de iptables tras reinicio"
    cat templates/firewall > /etc/network/if-pre-up.d/firewall
    chmod +x /etc/network/if-pre-up.d/firewall
    say_done
}

##############################################################################################################

function install_fail2ban() {
    clear
    show_banner
    # para eliminar una regla de fail2ban en iptables utilizar:
    # iptables -D fail2ban-ssh -s IP -j DROP
    write_title "9. Instalar Sendmail y fail2ban"
    apt-get install sendmail
    apt-get install fail2ban
    say_done
}

##############################################################################################################

function install_php() {
    clear
    show_banner
    write_title "10. Instalar PHP 5 + Apache 2"
    apt-get install php5 php5-cli php-pear libapache2-mod-php5 php5-mcrypt 
    apt-get install build-essential php5-dev libaio1
    apt-get install alien
    echo -n " reemplazando archivo de configuración php.ini..."
    cp templates/php /etc/php5/apache2/php.ini; echo " OK"
    service apache2 restart
    say_done
}

##############################################################################################################

function install_modsecurity() {
    clear
    show_banner
    write_title "11. Instalar ModSecurity"
    apt-get install libxml2 libxml2-dev libxml2-utils
    apt-get install libaprutil1 libaprutil1-dev
    apt-get install libapache2-mod-security2

    service apache2 restart
    say_done
}

##############################################################################################################

function install_owasp_core_rule_set() {
    clear
    show_banner
    write_title "12. Instalar OWASP ModSecurity Core Rule Set"

    echo -n " descargar........................................................ "
    uri_part1='http://pkgs.fedoraproject.org/repo/pkgs/mod_security_crs/'
    uri_part2='modsecurity-crs_2.2.5.tar.gz'
    uri_part3='aaeaa1124e8efc39eeb064fb47cfc0aa/modsecurity-crs_2.2.5.tar.gz'
    wget $uri_part1/$uri_part2/$uri_part3; echo "OK"

    echo -n " desempaquetar.................................................... "
    tar -xzf modsecurity-crs_2.2.5.tar.gz; echo "OK"

    echo -n " mover............................................................ "
    cp -R modsecurity-crs_2.2.5/* /etc/modsecurity/; echo "OK"

    echo -n " eliminar archivos temporales..................................... "
    rm modsecurity-crs_2.2.5.tar.gz
    rm -R modsecurity-crs_2.2.5; echo "OK"

    echo -n " configurar....................................................... "
    from_path="/etc/modsecurity/modsecurity_crs_10_setup.conf.example"
    to_path="/etc/modsecurity/modsecurity_crs_10_setup.conf"
    mv $from_path $to_path

    for archivo in /etc/modsecurity/base_rules/*
        do ln -s $archivo /etc/modsecurity/activated_rules/
    done

    for archivo in /etc/modsecurity/optional_rules/*
        do ln -s $archivo /etc/modsecurity/activated_rules/
    done
    echo "OK"

    sed s/SecRuleEngine\ DetectionOnly/SecRuleEngine\ On/g /etc/modsecurity/modsecurity.conf-recommended > salida
    mv salida /etc/modsecurity/modsecurity.conf
    
    echo 'SecServerSignature "AntiChino Server 1.0.4 LS"' >> /etc/modsecurity/modsecurity_crs_10_setup.conf
    echo 'Header set X-Powered-By "Plankalkül 1.0"' >> /etc/modsecurity/modsecurity_crs_10_setup.conf
    echo 'Header set X-Mamma "Mama mia let me go"' >> /etc/modsecurity/modsecurity_crs_10_setup.conf

    a2enmod headers
    a2enmod security2
    service apache2 restart
    say_done
}

##############################################################################################################

function configure_apache() {
    clear
    show_banner
    write_title "13. Finalizar configuración y optimización de Apache"
    cp templates/apache /etc/apache2/apache2.conf
    echo " -- habilitar ModRewrite"
    a2enmod rewrite
    service apache2 restart
    say_done
}

##############################################################################################################

function install_modevasive() {
    clear
    show_banner
    write_title "14. Instalar ModEvasive"
    echo -n " Indique e-mail para recibir alertas: "; read inbox
    apt-get install libapache2-mod-evasive
    mkdir /var/log/mod_evasive
    chown www-data:www-data /var/log/mod_evasive/
    sed s/MAILTO/$inbox/g templates/mod-evasive > /etc/apache2/mods-available/mod-evasive.conf
    a2enmod mod-evasive
    service apache2 restart
    say_done
}

##############################################################################################################

function config_fail2ban() {
    clear
    show_banner
    write_title "15. Finalizar configuración de fail2ban"
    sed s/MAILTO/$inbox/g templates/fail2ban > /etc/fail2ban/jail.local
    cp /etc/fail2ban/jail.local /etc/fail2ban/jail.conf
    /etc/init.d/fail2ban restart
    say_done
}

##############################################################################################################

function tunning_bashrc() {
    clear
    show_banner
    write_title "16. Reemplazar .bashrc"
    cp templates/bashrc /home/$username/.bashrc
    chown $username:$username /home/$username/.bashrc
    say_done
}

##############################################################################################################

function additional_packages(){
    clear
    show_banner
    write_title "17. Instalar PHPUnit";
    pear config-set auto_discover 1
    mv phpunit /usr/share/phpunit
    echo include_path = ".:/usr/share/phpunit:/usr/share/phpunit/PHPUnit" >> /etc/php5/apache2/php.ini
    echo include_path = ".:/usr/share/phpunit:/usr/share/phpunit/PHPUnit" >> /etc/php5/cli/php.ini
    service apache2 restart
    say_done
}

##############################################################################################################

function installing_oracleinstantclient(){
    clear
    show_banner
    write_title "18. Instalar OracleInstantClient";
    alien -i oracle/oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm
    alien -i oracle/oracle-instantclient12.1-devel-12.1.0.2.0-1.x86_64.rpm
    ln -s /usr/lib/oracle/12.1/client64/lib/libclntsh.so.12.1 /usr/lib/oracle/12.1/client64/lib/libclntsh.so
    echo "Cuando le solicite el ORACLE_HOME, ingresen este: instantclient,/usr/lib/oracle/12.1/client64/lib"
    pecl install oci8-2.0.10
    cp templates/oci8 /etc/php5/apache2/conf.d/oci8.ini
    service apache2 restart
    say_done
}

##############################################################################################################

function installing_composer(){
    clear
    show_banner
    write_title "19. Instalar Curl y Composer";
    apt-get install curl
    curl -sS https://getcomposer.org/installer | php
    mv composer.phar /usr/local/bin/composer
    say_done
}

##############################################################################################################

function installing_git(){
    clear
    show_banner
    write_title "20. Instalar Git";
    apt-get install git
}

##############################################################################################################

function final_step() {
    write_title "21. Finalizar deploy"
    echo -n " ¿Ha podido conectarse por SHH como $username? (y/n) "
    read respuesta
    if [ "$respuesta" == "y" ]; then
        reboot
    else
        echo "El servidor NO será reiniciado y su conexión permanecerá abierta."
        echo "Bye."
    fi
}

##############################################################################################################

function instalar_yii(){
    clear
    show_banner
    write_title "1. Descargar e Instalar Yii";
    wget "https://github.com/yiisoft/yii/releases/download/1.1.17/yii-1.1.17.467ff50.tar.gz"
    tar -xzvf yii-1.1.17.467ff50.tar.gz
    mv yii-1.1.17.467ff50/ /var/www/html/Yii 
    say_done
}

##############################################################################################################

function clonar_siat(){
    clear
    show_banner
    write_title "2. Clonar Proyecto SIAT";
    echo -e "\e[93m[?]\e[00m Ingrese su usuario bitbucket: "; read bitbucket_user
    git clone https://$bitbucket_user@bitbucket.org/teamsiat/siatapp.git /var/www/html/siatapp
    git --git-dir=/var/www/html/siatapp/.git --work-tree=/var/www/html/siatapp/ fetch
    git --git-dir=/var/www/html/siatapp/.git --work-tree=/var/www/html/siatapp/ checkout desarrollo
    . /var/www/html/siatapp/etc/comandos/permisos_carpetas.sh
    say_done
}

##############################################################################################################

function menu_ssh(){
clear
show_banner

echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
echo -e "\e[93m[+]\e[00m Usuarios SSH"
echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
echo ""
echo "1. Agregar Usuarios"
echo "2. Eliminar Usuarios"
echo "3. Salir"
echo	
read choice

case $choice in

1)
agregar_usuario_ssh
give_instructions_SSH
final_step_SSH
;;

2)
eliminar_usuario_ssh
;;

3)
break
;;

esac
}

##############################################################################################################

function agregar_usuario_ssh(){
    clear
    show_banner
    write_title "Agregar Usuario";
    echo -e "\e[93m[?]\e[00m Ingrese el usuario a agregar: "; read new_usuario_ssh
    adduser $new_usuario_ssh
    usermod -a -G sudo $new_usuario_ssh
    sed -i s/USERNAME/$new_usuario_ssh" USERNAME"/g /etc/ssh/sshd_config
    sed -i s/'PasswordAuthentication no'/'PasswordAuthentication yes'/g /etc/ssh/sshd_config
    service ssh restart    
    say_done
}

##############################################################################################################

function eliminar_usuario_ssh(){
    clear
    show_banner
    write_title "Eliminar Usuario";
    echo -e "\e[93m[?]\e[00m Ingrese el usuario a eliminar: "; read delete_usuario_ssh
    sed -i s/" "$delete_usuario_ssh/''/g /etc/ssh/sshd_config
    service ssh restart    
    say_done
}

##############################################################################################################

function give_instructions_SSH() {
    clear
    show_banner
    serverip=$(__get_ip)
    write_title "4. Generación de llave RSA en su ordenador local"
    echo " *** SI NO TIENE UNA LLAVE RSA PÚBLICA EN SU ORDENADOR, GENERE UNA ***"
    echo "     Siga las instrucciones y pulse INTRO cada vez que termine una"
    echo "     tarea para recibir una nueva instrucción"
    echo " "
    echo "     EJECUTE LOS SIGUIENTES COMANDOS:"
    echo -n "     a) ssh-keygen (Le va a solicitar un passphrase, es una clave interna de su key, recuerde esta clave)"; read foo4
    echo -n "     b) ssh-copy-id '$new_usuario_ssh@$serverip' (Le va a solicitar la contraseña del usuario $new_usuario_ssh)"; read foo5
    echo -n "     b) ssh-add (Es posible que le solicite el passphrase que indicó en el primer paso)"; read foo6
    say_done
}
##############################################################################################################

function final_step_SSH() {
    write_title "21. Finalizar deploy"
    echo -n " ¿Ha podido conectarse por SHH como $new_usuario_ssh? (y/n) "
    read respuesta
    if [ "$respuesta" == "y" ]; then
    	sed -i s/'PasswordAuthentication yes'/'PasswordAuthentication no'/g /etc/ssh/sshd_config
    	service ssh restart
	say_done    
    else
        echo "El servidor NO será reiniciado y su conexión permanecerá abierta."
        echo "Bye."
    fi
}
##############################################################################################################

clear
show_banner

echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
echo -e "\e[93m[+]\e[00m Comenzemos con la configuración"
echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
echo ""
echo "1. Start Deploy Production Environment"
echo "2. Instalar y Configurar Proyecto SIAT"
echo "3. Usuarios SSH"
echo "4. Salir"
echo

read choice

case $choice in

1)
check_root
config_host
sysupdate
set_new_user
give_instructions
permission_rsa
ssh_reconfigure
set_iptables_rules
create_iptable_script
install_fail2ban
install_php
install_modsecurity
install_owasp_core_rule_set
configure_apache
install_modevasive
config_fail2ban
tunning_bashrc
additional_packages
installing_oracleinstantclient
installing_composer
installing_git
final_step
;;

2)
instalar_yii
clonar_siat
;;

3)
menu_ssh
;;

4)
break;;


esac






















